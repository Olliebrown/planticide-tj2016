﻿using UnityEngine;
using System.Collections;

public class FlowerBit : FlowerScript {

	public Sprite[] options = new Sprite[4];
	public int active = 0;

	public bool isFlipped = false;

	protected override void grow() {
		applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [active], isFlipped);
	}

	public void setActiveSprite(int newActive, bool flipped = false) {
		isFlipped = flipped;
		int prevActive = active;
		active = newActive;

		applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [prevActive], flipped);

		// Set to proper sprites
		child = gameObject.transform.GetChild (0).gameObject;
		childSpriteRender = child.GetComponent<SpriteRenderer> ();
		childSpriteRender.flipX = flipped;
		childSpriteRender.sprite = options [prevActive];
		updateAlpha (childSpriteRender, 1.0f);

		spriteRender = gameObject.GetComponent<SpriteRenderer> ();
		spriteRender.sprite = options [active];
		spriteRender.flipX = flipped;
		updateAlpha (spriteRender, 0.0f);

		// Prepare to fade and request update
		alpha = 0.0f;
		updatingNeeded = true;
	}

	// Provide specialized fixed update logic
	protected override void fixedTick() {
		
		if (alpha < 1.0f) {
			blendStep ();
		} else {
			resetBlend ();
			updatingNeeded = false;
			applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [active], isFlipped);
		}
	}
}
