﻿using UnityEngine;
using System.Collections;

public class CactusModularGrowth : CactusGrowth {

	public GameObject cactusChild;

	public float posOffset;
	public float[] angleOffset = new float[2];
	public float angleDelta;

	protected override void matureGrow() {
		if (cactusChild == null) {
			return;
		}

		if (childCount < maxChildCount && depth < maxDepth) {
			GameObject newChild = spawnChild (cactusChild);
			float curTime = Time.time;

			newChild.GetComponent<GrowablePlant> ().birth = curTime; 
			newChild.GetComponent<GrowablePlant> ().lastGrowth = curTime;

			// Pick a random angle above parent
			float angle = angleOffset[transform.childCount-2] + ((Random.value*angleDelta) - (angleDelta/2.0f));
			if (angleDelta == 0) {
				angle = transform.eulerAngles.z+90;
			}
			float angleRad = Mathf.Deg2Rad * angle;

			// Setup transform to position and orient child
			newChild.transform.position = transform.position;
			newChild.transform.Translate((new Vector3 (Mathf.Cos (angleRad)*0.8f, Mathf.Sin (angleRad), 0.0f))*posOffset);
			newChild.transform.Rotate(new Vector3(0.0f, 0.0f, -90.0f+angle));
		}
	}
}