﻿using UnityEngine;
using System.Collections;

/* Class that handles flower-specific processing
Child of BlendablePlant */
public abstract class FlowerScript : BlendablePlant {
	//Working storage status value, set in updateStatus function
	private int wsStatus;

	// Use this for initialization
	//Run Start function in parent script
	//Creates plant with flower limits and initializes issues to zero
	public override void init () {

		if (gameObject.name.Contains ("Frond")) {
			mySortLevel = transform.parent.gameObject.GetComponent<SpriteRenderer> ().sortingOrder - 1;
		} else if (gameObject.name.Contains ("Bud")) {
			mySortLevel = transform.parent.gameObject.GetComponent<SpriteRenderer> ().sortingOrder + 1;
		} else {
			mySortLevel++;
		}

		//Run Start function in parent script
		base.init();
		//Creates plant with flower limits
		//Status, Sun Limit, Water Limit, Soil Limit
		instanciatePlant(0, 7, 7, 3);

	}
	
	// Update is called once per frame
	//Run Update function in parent script
	//Calculates issue values and end result status level
	void Update () {
		mainUpdate();
		updateStatus();
	}

	/* Called from Update function
	Adds each issue element and subtracts 2
	Set status variable to result value */
	private void updateStatus (){

		//Add each issue element and subtracts 2
		wsStatus = getIssues () [0] + getIssues () [1] + getIssues () [2] - 1;

		if (wsStatus < 0) {
			wsStatus = 0;
		} else if (wsStatus > 2) {
			wsStatus = 2;
		}

		//Set status variable to result value
		setStatus(wsStatus);
	}
}
