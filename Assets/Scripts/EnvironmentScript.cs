﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/* Class that handles all storage and processing of the environment
Holds weather, soil and water state
Processes variables based on weather */
public class EnvironmentScript : MonoBehaviour {
	[SerializeField]private GameObject weatherObject;
	[SerializeField]private Sprite sunSky;
	[SerializeField]private Sprite cloudSky;
	[SerializeField]private Sprite nightSky;
	[SerializeField]private Sprite cloudsLightLightSprite;
	[SerializeField]private Sprite cloudsLightHeavySprite;
	[SerializeField]private Sprite cloudsHeavyLightSprite;
	[SerializeField]private Sprite cloudsHeavyHeavySprite;
	[SerializeField]private AudioClip sunnySound;
	[SerializeField]private AudioClip cloudySound;
	[SerializeField]private AudioClip windySound;
	[SerializeField]private AudioClip rainySound;
	[SerializeField]private AudioClip nightSound;
	//GameUIScript, set in Engine Inspector
	private GameUIScript gameUIScript;
	private Color wsOverlayColor;
	//Current weather conditions, set from UI buttons through setter method
	//1: Sunny; 2: Cloudy; 3: Windy; 4: Rainy; 5: Night
	private int weatherState;
	//Current richness of soil, set from plant dying through setter method
	private float soilState;
	//Current amount of water in soil, updated based on weather on Update
	private float waterState;

	// Use this for initialization
	//Initialize weather to sunny, soil and water levels to medium
	//Set gameUIScript variable
	void Start () {
		gameUIScript = GameObject.Find("GameUIObject").GetComponent<GameUIScript>();
		weatherState = 1;
		soilState = 2;
		waterState = 5;
	}
	
	// Update is called once per frame
	void Update (){
		if (PlantScript.isPaused) { return; }
		updateWeatherVars();
		weatherEffects();
	}

	/* Called within Update function
	Updates weather-related states based on current weather conditions */
	private void updateWeatherVars (){
		//If weather is rainy
		if (weatherState == 4) {
			//Increase water state by .1 per second
			waterState += .1f * Time.deltaTime;
			//If weather is not rainy
		}else{
			//Decrease water state by .1 per second
			waterState -= .1f * Time.deltaTime;
		}
		//If water state is less than 0, reset to 0 to avoid going negative
		if (waterState < 0) {
			waterState = 0;
		}
	}

	private void weatherEffects (){
		switch (weatherState) {
			case 1:
				spawnClouds (1);
				spawnRain (false);
				spawnWind (false);

				if (gameObject.GetComponent<AudioSource> ().clip != sunnySound) {
					gameObject.GetComponent<AudioSource> ().clip = sunnySound;
					gameObject.GetComponent<AudioSource> ().Play ();
				}
				break;
			case 2:
				spawnClouds(2);
				spawnRain(false);
				spawnWind(false);

				if (gameObject.GetComponent<AudioSource> ().clip != cloudySound) {
					gameObject.GetComponent<AudioSource> ().clip = cloudySound;
					gameObject.GetComponent<AudioSource> ().Play ();
				}
				break;
			case 3:
				spawnClouds(4);
				spawnRain(false);
				spawnWind(true);
				
				if (gameObject.GetComponent<AudioSource> ().clip != windySound) {
					gameObject.GetComponent<AudioSource> ().clip = windySound;
					gameObject.GetComponent<AudioSource> ().Play ();
				}
				break;
			case 4:
				spawnClouds(4);
				spawnRain(true);
				spawnWind(false);
				
				if (gameObject.GetComponent<AudioSource> ().clip != rainySound) {
					gameObject.GetComponent<AudioSource> ().clip = rainySound;
					gameObject.GetComponent<AudioSource> ().Play ();
				}
				break;
			case 5:
				spawnClouds(3);
				spawnRain(false);
				spawnWind(false);
				
				if (gameObject.GetComponent<AudioSource> ().clip != nightSound) {
					gameObject.GetComponent<AudioSource> ().clip = nightSound;
					gameObject.GetComponent<AudioSource> ().Play ();
				}
				break;
		}
	}

	private void spawnClouds (int cloudType)
	{
		switch (cloudType) {
			case 1:
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<SpriteRenderer>().sprite = cloudsLightLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<SpriteRenderer>().sprite = cloudsLightLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<SpriteRenderer>().sprite = cloudsLightLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<SpriteRenderer>().sprite = cloudsLightLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<SpriteRenderer>().sprite = cloudsLightLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<SpriteRenderer>().sprite = cloudsLightLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				break;
			case 2:
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<SpriteRenderer>().sprite = cloudsLightHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<SpriteRenderer>().sprite = cloudsLightHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<SpriteRenderer>().sprite = cloudsLightHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<SpriteRenderer>().sprite = cloudsLightHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<SpriteRenderer>().sprite = cloudsLightHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<SpriteRenderer>().sprite = cloudsLightHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				break;
			case 3:
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<SpriteRenderer>().sprite = cloudsHeavyLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<SpriteRenderer>().sprite = cloudsHeavyLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<SpriteRenderer>().sprite = cloudsHeavyLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<SpriteRenderer>().sprite = cloudsHeavyLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<SpriteRenderer>().sprite = cloudsHeavyLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<SpriteRenderer>().sprite = cloudsHeavyLightSprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.1f);
				break;
			case 4:
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<SpriteRenderer>().sprite = cloudsHeavyHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<SpriteRenderer>().sprite = cloudsHeavyHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front").GetComponent<WeatherEffectScript>().setCloudSpeed(.2f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Front (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.2f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<SpriteRenderer>().sprite = cloudsHeavyHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<SpriteRenderer>().sprite = cloudsHeavyHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid").GetComponent<WeatherEffectScript>().setCloudSpeed(.2f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Mid (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.2f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<SpriteRenderer>().sprite = cloudsHeavyHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<SpriteRenderer>().sprite = cloudsHeavyHeavySprite;
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back").GetComponent<WeatherEffectScript>().setCloudSpeed(.2f);
				weatherObject.transform.FindChild("Clouds").FindChild("Clouds-Back (1)").GetComponent<WeatherEffectScript>().setCloudSpeed(.2f);
				break;
		}
	}

	private void spawnRain (bool rainEffect){
		weatherObject.transform.FindChild("Rain").gameObject.SetActive(rainEffect);
	}

	private void spawnWind (bool windEffect){
		weatherObject.transform.FindChild("Wind").gameObject.SetActive(windEffect);
	}

	/* Setter method - called when weather UI button is pushed
	Takes input from button push depending on which one is pushed
	Sets weather state from input and updates UI element*/
	public void setWeatherState (int aWeatherState){
		//Set weather state from input and update weather text UI
		weatherState = aWeatherState;
		gameUIScript.setWeatherStateText("Weather: " + string.Concat(weatherState));
		changeBackgroundVisual();
	}

	/* Setter method - called when plant dies, accepting input based on how large the plant was
	Increases soil state based on input and updates UI element*/
	public void setSoilState (float aSoilState){
		//Set soil state from input and update soil text UI
		soilState = aSoilState;

		if (soilState > 10) {
			soilState = 10;
		}

		gameUIScript.setSoilStateText("Soil: " + string.Concat(soilState));
	}

	private void changeBackgroundVisual (){
		switch (weatherState) {
			case 1:
				changeSpecificBackgroundVisual(sunSky, true, false, 0f);
				break;
			case 2:
				changeSpecificBackgroundVisual(cloudSky, true, false, .2f);
				break;
			case 3:
				changeSpecificBackgroundVisual(cloudSky, true, false, .2f);
				break;
			case 4:
				changeSpecificBackgroundVisual(cloudSky, false, false, .4f);
				break;
			case 5:
				changeSpecificBackgroundVisual(nightSky, false, true, .6f);
				break;
		}
	}

	private void changeSpecificBackgroundVisual (Sprite aSky, bool aSun, bool aMoon, float overlayOpacity){
		weatherObject.transform.FindChild("Sky").GetComponent<SpriteRenderer>().sprite = aSky;
		weatherObject.transform.FindChild("Sun").gameObject.SetActive(aSun);
		weatherObject.transform.FindChild("Moon").gameObject.SetActive(aMoon);
		wsOverlayColor = weatherObject.transform.FindChild("BackgroundOverlay").GetComponent<SpriteRenderer>().color;
		wsOverlayColor.a = overlayOpacity;
		weatherObject.transform.FindChild("BackgroundOverlay").GetComponent<SpriteRenderer>().color = wsOverlayColor;
	}

	//Getter method for weather state variable
	public int getWeatherState (){
		return weatherState;
	}
	
	//Getter method for soil state variable
	public float getSoilState (){
		return soilState;
	}

	//Getter method for water state variable
	public float getWaterState (){
		return waterState;
	}
}
