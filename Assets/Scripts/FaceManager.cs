﻿using UnityEngine;
using System.Collections;

public class FaceManager : MonoBehaviour {

	public static int BLANK_FACE = 0;
	public static int DECEASED_FACE = 1;
	public static int HAPPY_FACE = 2;
	public static int NEUTRAL_FACE = 3;
	public static int SAD_FACE = 4;

	public Sprite[] faceSprites = new Sprite[4];
	private int activeFace;
	private SpriteRenderer spriteRender;

	void Awake() {
		spriteRender = GetComponent<SpriteRenderer> ();
	}

	public int ActiveFace {
		get {
			return activeFace;
		}
		set {
			if (value >= 0 && value < faceSprites.Length) {
				activeFace = value;
				spriteRender.sprite = faceSprites [activeFace];
			}
		}
	}
}
