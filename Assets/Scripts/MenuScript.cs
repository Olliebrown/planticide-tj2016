﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/* Class that handles all logic regarding the main menu system
Handles UI button handling */
public class MenuScript : MonoBehaviour {
	//Canvas for instructions canvas, set in Engine Inspector
	[SerializeField]private Canvas instructionsCanvas;
	//Canvas for about canvas, set in Engine Inspector
	[SerializeField]private Canvas aboutCanvas;
	//Canvas for main menu canvas, set in Engine Inspector
	[SerializeField]private Canvas mainmenuCanvas;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	/* Called when UI button is pushed
	Based on pushed button, performs action */
	public void buttonPush (string buttonID){
		
		
		switch (buttonID) {
			//Start button pushed, load main game scene
			case "start":
				SceneManager.LoadScene(1);
				break;
			//Instructions button pushed, overlay instructions screen
			case "instructions":
				instructionsCanvas.gameObject.SetActive(true);
				break;
			//About button pushed, overlay about screen
			case "about":
				aboutCanvas.gameObject.SetActive(true);
				break;
			//Quit button pushed, quit application
			case "quit":
				Application.Quit();
				break;
			//Back button pushed, return to main menu
			case "back":
				instructionsCanvas.gameObject.SetActive(false);
				aboutCanvas.gameObject.SetActive(false);
				break;
		}
	}
}
