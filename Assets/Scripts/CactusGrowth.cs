﻿using UnityEngine;
using System.Collections;

public class CactusGrowth : CactusScript {

	public Sprite[] stages = new Sprite[5];
	public GameObject cactusFacePrefab = null;

	protected FaceManager cactusFace = null;
	private static Color faceColor = new Color(0/255.0f, 149/255.0f, 96/255.0f);

	// Override growth behavior
	protected override void grow () {

		applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [curAge-1]);

		if (curAge <= maturity) {
			// Set to next sprites
			childSpriteRender.sprite = stages [curAge - 1];
			updateAlpha (childSpriteRender, 1.0f);

			spriteRender.sprite = stages [curAge];
			updateAlpha (spriteRender, 0.0f);

			// Prepare to fade and request update
			alpha = 0.0f;
			updatingNeeded = true;

		} else {
			if(cactusFacePrefab != null && cactusFace == null) makeFaces ();

			alpha = 1.0f;
			updatingNeeded = false;
			matureGrow ();
		}
	}

	protected void makeFaces() {
		GameObject cactusFaceObj = Instantiate (cactusFacePrefab);
		CousinGameObject = cactusFaceObj;

		cactusFaceObj.transform.position = new Vector3(transform.position.x, transform.position.y+2, transform.position.z);
		cactusFaceObj.GetComponent<SpriteRenderer> ().sortingOrder = mySortLevel + 1;
		cactusFaceObj.GetComponent<SpriteRenderer> ().color = faceColor;

		cactusFace = cactusFaceObj.GetComponent<FaceManager> ();
		cactusFace.ActiveFace = 4;
	}

	protected virtual void matureGrow() {
		if (cactusFace != null) {
			switch (getStatus()) {
				case 0: // growing
					cactusFace.ActiveFace = FaceManager.HAPPY_FACE;
					break;

				case 1: // neutral
					cactusFace.ActiveFace = FaceManager.NEUTRAL_FACE;
					break;

				case 2: // dying
					cactusFace.ActiveFace = FaceManager.SAD_FACE;
					break;

				case 3: // dead
					cactusFace.ActiveFace = FaceManager.DECEASED_FACE;
					break;
			}
		}
	}

	// Provide specialized fixed update logic
	protected override void fixedTick() {

		if (alpha < 1.0f) {
			blendStep ();
		} else {
			resetBlend ();
			updatingNeeded = false;
			applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [curAge]);
		}
	}

}
