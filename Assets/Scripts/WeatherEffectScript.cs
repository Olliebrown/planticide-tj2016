﻿using UnityEngine;
using System.Collections;

public class WeatherEffectScript : MonoBehaviour {
	[SerializeField]private string effectType;
	private float cloudSpeed;

	// Use this for initialization
	void Start () {
		cloudSpeed = .1f;
	}
	
	// Update is called once per frame
	void Update () {
		if (PlantScript.isPaused) { return; }
		moveEffect();
	}

	private void moveEffect ()
	{
		switch (effectType) {
			case "wind-front":
				transform.position += new Vector3 (15 * Time.deltaTime, 0, 0);

				if (transform.localPosition.x > 34f) {
					transform.localPosition = new Vector3(-55.7f, transform.localPosition.y, transform.localPosition.z);
				}
				break;
			case "wind-mid":
				transform.position += new Vector3 (12.5f * Time.deltaTime, 0f, 0f);

				if (transform.localPosition.x > 29.5f) {
					transform.localPosition = new Vector3(-47.2f, transform.localPosition.y, transform.localPosition.z);
				}
				break;
			case "wind-back":
				transform.position += new Vector3 (10 * Time.deltaTime, 0, 0);

				if (transform.localPosition.x > 27.1f) {
					transform.localPosition = new Vector3(-37.2f, transform.localPosition.y, transform.localPosition.z);
				}
				break;
			case "rain-front":
				transform.position += new Vector3 (0, -40 * Time.deltaTime, 0);

				if (transform.localPosition.y < -22.0f) {
					transform.localPosition = new Vector3(transform.localPosition.x, 36.6f, transform.localPosition.z);
				}
				break;
			case "rain-mid":
				transform.position += new Vector3 (0, -30 * Time.deltaTime, 0);

				if (transform.localPosition.y < -18f) {
					transform.localPosition = new Vector3(transform.localPosition.x, 30f, transform.localPosition.z);
				}
				break;
			case "rain-back":
				transform.position += new Vector3 (0, -20 * Time.deltaTime, 0);

				if (transform.localPosition.y < -16.5f) {
					transform.localPosition = new Vector3(transform.localPosition.x, 24f, transform.localPosition.z);
				}
				break;
			case "cloud-front":
				transform.localPosition += new Vector3 (cloudSpeed * 1.5f * Time.deltaTime, 0f, 0f);

				if (transform.localPosition.x > 40f) {
					transform.localPosition = new Vector3(-75f, transform.localPosition.y, transform.localPosition.z);
				}
				break;
			case "cloud-mid":
				transform.localPosition += new Vector3 (cloudSpeed * Time.deltaTime, 0f, 0f);

				if (transform.localPosition.x > 35.7f) {
					transform.localPosition = new Vector3(-67f, transform.localPosition.y, transform.localPosition.z);
				}
				break;
			case "cloud-back":
				transform.localPosition += new Vector3 (cloudSpeed * .5f * Time.deltaTime, 0f, 0f);

				if (transform.localPosition.x > 33.0f) {
					transform.localPosition = new Vector3(-57.9f, transform.localPosition.y, transform.localPosition.z);
				}
				break;
		}
	}

	public void setCloudSpeed(float aCloudSpeed){
		cloudSpeed = aCloudSpeed;
	}
}
