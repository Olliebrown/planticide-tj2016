﻿using UnityEngine;
using System.Collections;

public abstract class BlendablePlant : GrowablePlant {

	// Speed of the blend, scales to apply at each in-between stage
	public float blendSpeed = 4;
	protected float alpha;

	// References to the renderers on this and its child
	protected SpriteRenderer spriteRender = null;
	protected GameObject child = null;
	protected SpriteRenderer childSpriteRender = null;

	// Set the references (make sure this still gets called)
	public override void init() {
		base.init ();
		spriteRender = gameObject.GetComponent<SpriteRenderer> ();
		child = gameObject.transform.GetChild (0).gameObject;
		childSpriteRender = child.GetComponent<SpriteRenderer> ();

		if (spriteRender == null) {
			Debug.Log (name + " does not have a sprite renderer.");
		}

		if (childSpriteRender == null) {
			Debug.Log (name + " does not have a CHILD sprite renderer.");
		}

		spriteRender.sortingOrder = mySortLevel;
		childSpriteRender.sortingOrder = mySortLevel;
	}

	// Finish a blend
	protected void resetBlend() {

		updateAlpha (childSpriteRender, 0.0f);
		updateAlpha (spriteRender, 1.0f);

		alpha = 0.0f;
	}

	// Make one step in a blend
	protected void blendStep() {
		updateAlpha (childSpriteRender, 1.0f - alpha);
		updateAlpha (spriteRender, alpha);
		alpha += blendSpeed * 0.01f;
	}

	// Helper funciton to update the alpha of a SpriteRenderer
	protected void updateAlpha(SpriteRenderer SR, float newa) {
		Color C1 = SR.color;
		C1.a = newa;
		SR.color = C1;
	}
}
