﻿using UnityEngine;
using System.Collections;

public class MultispriteManagement : MonoBehaviour {

	public GameObject[] spriteObjects = new GameObject[1];
	public SpriteRenderer[] renderers;

	void Start() {
		renderers = new SpriteRenderer[spriteObjects.Length];

		int i = 0;
		foreach (GameObject obj in spriteObjects) {
			renderers [i] = obj.GetComponent<SpriteRenderer> ();
			i++;
		}
	}

	// Change the alpha of all the sprites
	public void setAlpha(float newAlpha) {
		foreach (SpriteRenderer SR in renderers) {
			updateAlpha (SR, newAlpha);
		}
	}

	// Helper funciton to update the alpha of a SpriteRenderer
	protected void updateAlpha(SpriteRenderer SR, float newa) {
		Color C1 = SR.color;
		C1.a = newa;
		SR.color = C1;
	}
}
