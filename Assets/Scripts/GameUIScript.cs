﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/* Class that handles all UI elements in game
Creates plants based off buttons, changes weather based off buttons, handles selected plant display */
public class GameUIScript : MonoBehaviour {
	//Game Object linked to cactus prefab, set in Engine Inspector
	[SerializeField]private GameObject cactus;
	//Game Object linked to oak tree prefab, set in Engine Inspector
	[SerializeField]private GameObject oakTree;
	[SerializeField]private GameObject oakTree2;
	//Game Object linked to sunflower prefab, set in Engine Inspector
	[SerializeField]private GameObject sunFlower;
	//Game Object linked to maple tree prefab, set in Engine Inspector
	[SerializeField]private GameObject mapleTree;
	[SerializeField]private GameObject mapleTree2;
	//Entire object for UI element when no plant is selectd, set in Engine Inspector
	[SerializeField]private GameObject notSelectedText;
	//Entire object for UI element when plant is selected, set in Engine Inspector
	[SerializeField]private GameObject selectedText;
	[SerializeField]private GameObject weatherMenu;
	[SerializeField]private GameObject plantMenu;
	//Text linked to time UI element, set in Engine Inspector
	[SerializeField]private Text timeText;
	//Text linked to sun level UI element of selected plant, set in Engine Inspector
	[SerializeField]private Text sunLevelText;
	//Text linked to water level UI element of selected plant, set in Engine Inspector
	[SerializeField]private Text waterLevelText;
	//Text linked to soil level UI element of selected plant, set in Engine Inspector
	[SerializeField]private Text soilSupportText;
	//Text linked to status UI element of selected plant, set in Engine Inspector
	[SerializeField]private Text statusText;
	//Text object displayed in UI for weather state, set in Engine Inspector
	[SerializeField] private Text weatherStateText;
	//Text object displayed in UI for soil richness, set in Engine Inspector
	[SerializeField] private Text soilStateText;
	//PlantScript of selected plant, set at setSectedPlant function
	private PlantScript selectedPlantScript;
	private GameObject animatingMenu;
	//Main camera, set at Start function
	private Camera mainCamera;
	//Position of mouse in world coordinates, set at getMousePosition function
	private Vector3 mousePosition;
	//Selected plant Game Object, set at setSectedPlant function
	private GameObject selectedPlant;
	//String description of status of sun for plant, set at convertSunLevel function
	private string convertedSunLevel;
	//String description of status of water for plant, set at convertWaterLevel function
	private string convertedWaterLevel;
	//String description of status of soil for plant, set at convertSoilStatus function
	private string convertedSoilSupport;
	//String description of status of plant, set at convertStatus function
	private string convertedStatus;
	//Calculate difference between current sun level stat and sun limit, set at convertSunLevel function
	private float sunDifference;
	//Calculate difference between current water level stat and water limit, set at convertWaterLevel function
	private float waterDifference;
	//Calculate difference between current soil level stat and soil limit, set at convertSoilStatus function
	private float soilDifference;
	private float plantMenuAnimateStart;
	private float weatherMenuAnimateStart;
	private float animateDeltaTime;
	private bool animatePlantMenu = false;
	private bool animateWeatherMenu = false;
	private bool plantMenuState = false;
	private bool weatherMenuState = false;
	private bool deletePlant = false;

	// Use this for initialization
	//Set main camera and deactivate selected text
	void Start () {
		mainCamera = Camera.main;
		selectedText.SetActive(false);
	}
	
	// Update is called once per frame
	//Update time text and update selected plant text
	void Update () {
		timeText.text = "Time: " + string.Concat(Mathf.Ceil(Time.time));

		updateSelectedPlantText();
		animateMenu();
		checkForQuit();
	}

	/* Called when plant button is pushed
	Accepts input of what plant is to be created
	Creates plant based on input at mouse position*/
	public void createPlant (string plant){
		//Get mouse position
		getMousePosition();
		
		switch (plant) {
			//Cactus button pushed, create cactus
			case "cactus":
				Instantiate(cactus, mousePosition, Quaternion.identity);
				break;
			//Oak tree button pushed, create oak tree
		case "oaktree":
				if (Random.value > 0.5) {
					Instantiate (oakTree, mousePosition, Quaternion.identity);
				} else {
					Instantiate (oakTree2, mousePosition, Quaternion.identity);
				}
				break;
			//Sunflower button pushed, create sunflower
			case "sunflower":
				Instantiate(sunFlower, mousePosition, Quaternion.identity);
				break;
			//Maple tree button pushed, create maple tree
			case "mapletree":
				if (Random.value > 0.5) {
					Instantiate (mapleTree, mousePosition, Quaternion.identity);
				} else {
					Instantiate (mapleTree2, mousePosition, Quaternion.identity);
				}
				break;
		}
	}

	/* Called every frame
	If a plant is selected:
		Convert current plant stats and limits to string description
		Update selected plant UI elements */
	private void updateSelectedPlantText (){
		//If a plant is selected
		if (selectedPlant != null) {
			//Convert current plant stats and limits to string description
			convertPlantStats();
			
			//Update each text UI element with new string descriptions
			sunLevelText.text = "Sun Level: " + convertedSunLevel;
			waterLevelText.text = "Water Level: " + convertedWaterLevel;
			soilSupportText.text = "Soil Support: " + convertedSoilSupport;
			statusText.text = "Status: " + convertedStatus;
		}
	}
	
	/* Called from updateSelectedPlantText function
	Called sub-functions to convert each current plant stat and limit to string description */
	private void convertPlantStats(){
		convertSunLevel();
		convertWaterLevel();
		convertSoilSupport();
		convertStatus();
	}

	/* Called from convertPlantStats function
	Converts current sun plant stat and limit to string description */
	private void convertSunLevel (){
		//Calculate difference between current sun level stat and sun limit
		sunDifference = selectedPlantScript.getSunLevel() - selectedPlantScript.getSunLimit ();
		
		//Set convertedSunLevel to string description based on calculated sunDifference
		//If current sun level is more than 1 higher than sun limit, set to "Good"
		if (sunDifference > 1) {
			convertedSunLevel = "Good";
		//If current sun level is between 0 and 1 higher than sun limit, set to "Danger"
		} else if (sunDifference <= 1 && sunDifference >= 0) {
			convertedSunLevel = "Danger";
		//If current sun level is between 0 and 1 lower than sun limit, set to "Bad"
		} else if (sunDifference < 0 && sunDifference >= -1) {
			convertedSunLevel = "Bad";
		//If current sun level is less than 1 lower than sun limit, set to "Horrible"
		}else{
			convertedSunLevel = "Horrible";
		}
	}

	/* Called from convertPlantStats function
	Converts current water plant stat and limit to string description */
	private void convertWaterLevel(){
		//Calculate difference between current water level stat and water limit
		waterDifference = selectedPlantScript.getEnvironmentScript().getWaterState() - selectedPlantScript.getWaterLimit();

		//Set convertedWaterLevel to string description based on calculated waterDifference
		//If current water level is more than 1 higher than water limit, set to "Good"
		if (waterDifference > 1) {
			convertedWaterLevel = "Good";
		//If current water level is between 0 and 1 higher than water limit, set to "Danger"
		} else if (waterDifference <= 1 && waterDifference >= 0) {
			convertedWaterLevel = "Danger";
		//If current water level is between 0 and 1 lower than water limit, set to "Bad"
		} else if (waterDifference < 0 && waterDifference >= -1) {
			convertedWaterLevel = "Bad";
		//If current water level is less than 1 lower than water limit, set to "Horrible"
		}else{
			convertedWaterLevel = "Horrible";
		}
	}

	/* Called from convertPlantStats function
	Converts current soil plant stat and limit to string description */
	private void convertSoilSupport(){
		//Calculate difference between current soil level stat and soil limit
		soilDifference = selectedPlantScript.getEnvironmentScript().getSoilState() - selectedPlantScript.getSoilLimit();

		//Set convertedSoilLevel to string description based on calculated soilDifference
		//If current soil level is more than 1 higher than soil limit, set to "Good"
		if (soilDifference > 1) {
			convertedSoilSupport = "Good";
		//If current soil level is between 0 and 1 higher than soil limit, set to "Danger"
		} else if (soilDifference <= 1 && soilDifference >= 0) {
			convertedSoilSupport = "Danger";
		//If current soil level is between 0 and 1 lower than soil limit, set to "Bad"
		} else if (soilDifference < 0 && soilDifference >= -1) {
			convertedSoilSupport = "Bad";
		//If current soil level is less than 1 lower than soil limit, set to "Horrible"
		}else{
			convertedSoilSupport = "Horrible";
		}
	}

	/* Called from convertPlantStats function
	Converts current status plant stat to string description */
	private void convertStatus(){
		//Set convertedStatus to string description based on value
		//If status value is 2, set to "Dying"
		if (selectedPlantScript.getStatus() == 2) {
			convertedStatus = "Dying";
		//If status value is 1, set to "Static"
		} else if (selectedPlantScript.getStatus() == 1) {
			convertedStatus = "Static";
		//If status value is 0, set to "Growing"
		}else{
			convertedStatus = "Growing";
		}
	}

	/* Called from createPlant function
	Gets mouse position and converts to usable world coordinates */
	private void getMousePosition (){
		//Get mouse position in world points
		mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
		//Change z to 0 so plant spawns in the right depth
		mousePosition.z = 0;
	}

	private void animateMenu (){
		if (animatePlantMenu) {
			animatingMenu = plantMenu;
			animateDeltaTime = Time.time - plantMenuAnimateStart;
			
			if (plantMenuState) {
				animatePlantMenu = openMenu();
			} else {
				animatePlantMenu = closeMenu();
			}
		}
		if (animateWeatherMenu) {
			animatingMenu = weatherMenu;
			animateDeltaTime = Time.time - weatherMenuAnimateStart;
			
			if (weatherMenuState) {
				animateWeatherMenu = openMenu();
			} else {
				animateWeatherMenu = closeMenu();
			}
		}
	}

	private bool openMenu (){
		Transform buttonBkg = animatingMenu.transform.FindChild ("Bottom").FindChild ("ButtonBackground");
		Transform mainBkg = animatingMenu.transform.FindChild ("Top").FindChild ("MainBackground");
			
		if (animateDeltaTime < .25f) {
			mainBkg.localScale += new Vector3(1.4f * Time.deltaTime, 0f, 0f);
		} else if (animateDeltaTime < .5f) {
			buttonBkg.localScale += new Vector3(0f, 4f * Time.deltaTime, 0f);
			buttonBkg.position -= new Vector3(0f, 350f * Time.deltaTime, 0f);
		} else if (animateDeltaTime < .75f) {
			buttonBkg.localScale = new Vector3(1f, 1f, 1f);
			buttonBkg.localPosition = new Vector3(buttonBkg.localPosition.x, 64.3f, 0f);
			animatingMenu.transform.FindChild("Bottom").FindChild("Buttons").gameObject.SetActive(true);

			return false;
		}

		return true;
	}

	private bool closeMenu (){
		Transform buttonBkg = animatingMenu.transform.FindChild ("Bottom").FindChild ("ButtonBackground");
		Transform mainBkg = animatingMenu.transform.FindChild ("Top").FindChild ("MainBackground");

		if (animateDeltaTime < .1f) {
			animatingMenu.transform.FindChild("Bottom").FindChild("Buttons").gameObject.SetActive(false);
		} else if (animateDeltaTime < .35f) {
			buttonBkg.localScale -= new Vector3(0f, 4f * Time.deltaTime, 0f);
			buttonBkg.position += new Vector3(0f, 385f * Time.deltaTime, 0f);
		} else if (animateDeltaTime < .6f) {
			buttonBkg.localScale = new Vector3(0f, 0f, 0f);
			mainBkg.localScale -= new Vector3(1.4f * Time.deltaTime, 0f, 0f);
		} else {
			mainBkg.localScale = new Vector3(.65f, 1f, 1f);
			buttonBkg.localScale = new Vector3(1f, 0f, 1f);
			buttonBkg.localPosition = new Vector3(buttonBkg.localPosition.x, 149.6f, 0f);
			
			return false;
		}

		return true;
	}

	private void checkForQuit (){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			SceneManager.LoadScene(0);
		}
	}

	/* Called from PlantScript when plant is clicked
	Takes input as Game Object
	Sets selected plant variable to selected plant Game Object (Null when plant is deselected)
	If selected plant is null: Null plant script variable, Selected text UI, activate not selected text UI
	If selected plant has object: Set plant script variable to script attached to object, activate selected text UI, deactivate not selected text UI */
	public void setSelectedPlant (GameObject aSeletedPlant)
	{
		//Sets selected plant variable to input (Can be null when plant is deselected)
		selectedPlant = aSeletedPlant;

		//If selected plant is null: Null plant script variable, Selected text UI, activate not selected text UI 
		if (selectedPlant == null) {
			selectedPlantScript = null;
			selectedText.SetActive(false);
			notSelectedText.SetActive(true);
		//If selected plant has object: Set plant script variable to script attached to object, activate selected text UI, deactivate not selected text UI
		} else {
			selectedPlantScript = selectedPlant.GetComponent<PlantScript>();
			selectedText.SetActive(true);
			notSelectedText.SetActive(false);
		}
	}

	public void changeWeatherMenuState ()	{
		if (animateWeatherMenu == false) {
			animateWeatherMenu = true;
			weatherMenuState = !weatherMenuState;
			weatherMenuAnimateStart = Time.time;
		}
	}

	public void changePlantMenuState (){
		if (animatePlantMenu == false) {
			animatePlantMenu = true;
			plantMenuState = !plantMenuState;
			plantMenuAnimateStart = Time.time;
		}
	}

	public bool getDeletePlant (){
		return deletePlant;
	}

	public void setDeletePlant (){
		deletePlant = !deletePlant;
	}

	//Setter method to update weather state UI text
	public void setWeatherStateText(string aWeatherStateText){
		weatherStateText.text = aWeatherStateText;
	}
	
	//Setter method to update soil state UI text
	public void setSoilStateText(string aSoilStateText){
		soilStateText.text = aSoilStateText;
	}
}
