﻿using UnityEngine;
using System.Collections;

public class MapleGrowth : MapleScript {

	public Sprite[] stages = new Sprite[5];
	public GameObject cactusFacePrefab = null;
	private static Color faceColor = new Color(178/255.0f, 151/255.0f, 115/255.0f);

	protected FaceManager cactusFace = null;

	// Override growth behavior
	protected override void grow () {

		if (curAge > 0 && curAge <= hitBoxDims.Length) {
			applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [curAge - 1]);
		}

		if (curAge <= maturity) {
			// Set to next sprites
			childSpriteRender.sprite = stages [curAge - 1];
			updateAlpha (childSpriteRender, 1.0f);

			spriteRender.sprite = stages [curAge];
			updateAlpha (spriteRender, 0.0f);

			// Prepare to fade and request update
			alpha = 0.0f;
			updatingNeeded = true;

		} else {
			if(cactusFacePrefab != null && cactusFace == null) makeFaces ();

			alpha = 1.0f;
			updatingNeeded = false;
			matureGrow ();
		}
	}

	protected void makeFaces() {
		GameObject cactusFaceObj = Instantiate (cactusFacePrefab);
		CousinGameObject = cactusFaceObj;

		if (name.Contains ("2")) {
			cactusFaceObj.transform.position = new Vector3 (transform.position.x - 0.2f, transform.position.y + 3.0f, transform.position.z);
			cactusFaceObj.transform.localScale = new Vector3 (0.4f, 0.4f, 0.4f);
		} else {
			cactusFaceObj.transform.position = new Vector3 (transform.position.x, transform.position.y + 2, transform.position.z);
			cactusFaceObj.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
		}

		cactusFaceObj.GetComponent<SpriteRenderer> ().sortingOrder = mySortLevel + 1;
		cactusFaceObj.GetComponent<SpriteRenderer> ().color = faceColor;

		cactusFace = cactusFaceObj.GetComponent<FaceManager> ();
		cactusFace.ActiveFace = 4;
	}

	protected virtual void matureGrow() {
		if (cactusFace != null) {
			switch (getStatus()) {
			case 0: // growing
				cactusFace.ActiveFace = FaceManager.HAPPY_FACE;
				break;

			case 1: // neutral
				cactusFace.ActiveFace = FaceManager.NEUTRAL_FACE;
				break;

			case 2: // dying
				cactusFace.ActiveFace = FaceManager.SAD_FACE;
				break;

			case 3: // dead
				cactusFace.ActiveFace = FaceManager.DECEASED_FACE;
				break;
			}
		}
	}

	// Provide specialized fixed update logic
	protected override void fixedTick() {

		if (alpha < 1.0f) {
			blendStep ();
		} else {
			resetBlend ();
			updatingNeeded = false;

			if (curAge >= 0 && curAge < hitBoxDims.Length) {
				applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [curAge]);
			}
		}
	}

}
