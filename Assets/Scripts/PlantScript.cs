﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/* Class that handles all holding and processing of plant features
Parent of all types of plants
Hold stat levels and limits, calculates number of issues, etc */
public abstract class PlantScript : MonoBehaviour {
	//EnvironmentScript, set in init function
	private EnvironmentScript environmentScript;
	//GameUIScript, set in init function
	private GameUIScript gameUIScript;
	//Main Camera, set in init function
	private Camera mainCamera;
	//Mouse position in world coordinates, set in getMousePosition function
	private Vector3 mousePosition;
	// Set this to false if you don't want it to be dragable at birth
	public bool dragable = true;
	//Whether plant has been placed and is dragging or not, set on initialization and dragPlant function
	protected bool dragging = true;
	//Whether plant is active selected, set in OnMouseDown function
	private bool selected = false;
	//Current sun stat of plant, set in updateLevels function
	private float sunLevel = 10;
	//Lower limit of sun before issue is set, set on plant instanciate in instanciatePlant function
	private float sunLimit;
	//Lower limit of water before issue is set, set on plant instanciate in instanciatePlant function
	private float waterLimit;
	//Lower limit of soil before issue is set, set on plant instanciate in instanciatePlant function
	private float soilLimit;
	//Array holding whether there is an issue for sun, water and soil, set in updateIssues function
	private int[] issues = new int[3];

	//Current status of plant, set in setter function
	//3: Dead; 2: Dying; 1: Static; 0: Growing
	protected int status;
	protected float startedDying = 0;
	protected float deathCountdown = 10.0f;

	protected GameObject rootGameObject;
	protected GameObject cousinGameObject;

	public static bool isPaused = false;

	protected static int curSortLevel = 0;
	public int mySortLevel;
	public int occupiesLayers = 1;

	void Awake () {
		mySortLevel = curSortLevel;
		curSortLevel += occupiesLayers;
		rootGameObject = this.gameObject;
		cousinGameObject = null;
	}

	// Use this for initialization
	//Call init function
	void Start () {
		init();
	}

	/* Called when any plant is instanciated
	Sets script and camera values
	Initializes issues to zeros */
	public virtual void init(){
		environmentScript = GameObject.Find("EnvironmentState").GetComponent<EnvironmentScript>();
		gameUIScript = GameObject.Find("GameUIObject").GetComponent<GameUIScript>();
		mainCamera = Camera.main;
		dragging = dragable;
		//Initialize issues to zero
		issues[0] = 0;
		issues[1] = 0;
		issues[2] = 0;
	}
	
	// Update is called once per frame
	//Calls mainUpdate function
	void Update (){
		if (PlantScript.isPaused) { return; }
		mainUpdate();
	}

	/* Called on update of every plant
	Plant tracks mouse when first created
	Updates stat levels and issue array */
	public void mainUpdate () {

		// Check for death
		if (status == 2) {
			float elapsed = Time.time - startedDying;
			if (elapsed > deathCountdown) {
				setStatus (3);
			}
		}

		dragPlant();
		updateLevels();
		updateIssues();
	}

	/* Called when plant is created from child class
	Accepts and applies inputs for initial status and stat limits */
	protected void instanciatePlant (int aStatus, float aSunLimit, float aWaterLimit, float aSoilLimit){
		status = aStatus;
		sunLimit = aSunLimit;
		waterLimit = aWaterLimit;
		soilLimit = aSoilLimit;
	}

	public GameObject RootGameObject {
		get {
			return rootGameObject;
		}
		set {
			rootGameObject = value;
		}
	}

	public GameObject CousinGameObject {
		get {
			return cousinGameObject;
		}
		set {
			cousinGameObject = value;
		}
	}

	/* Called from update function
	If player has not placed plant, tracks mouse */
	private void dragPlant (){
		//If player has not yet placed plant
		if (dragging) {
			//Get current mouse position in world coordinates
			getMousePosition();
			//Set transform to mouse position
			transform.position = mousePosition;
			
			//If mouse button is clicked and plant is on ground, stop dragging, plant placed
			if (Input.GetMouseButtonDown(0) && transform.position.y < -11.75) {
				dragging = false;
				doneDragging ();
			}
		}
	}

	protected abstract void doneDragging();

	/* Called in Update function
	If sunny outside, increase sun stat
	If not sunny, decrease sun stat */
	private void updateLevels (){

		//If sunny weather
		if (environmentScript.getWeatherState () == 1) {
			//Increase sun stat by .1 every second
			sunLevel += .1f * Time.deltaTime;
		//If not sunny or cloudy weather 
		} else if (environmentScript.getWeatherState () != 2) {
			//Decrease sun stat by .1 every second
			sunLevel -= .1f * Time.deltaTime;
		}
	}

	/* Called from Update function
	Determines if curent plant stat is less than limit for sun, water and soil levels */
	private void updateIssues (){

		//If current sun level is less than limit
		if (getSunLevel () < getSunLimit()) {
			//Set sun issue element to 1
			issues [0] = 1;
		//If current sun level is greater than limit
		} else {
			//Set sun issue element to 0
			issues [0] = 0;
		}

		//If current water level is less than limit
		if (getEnvironmentScript().getWaterState() < getWaterLimit ()) {
			//Set water issue element to 1
			issues[1] = 1;
		//If current water level is greater than than limit
		} else {
			//Set water issue element to 0
			issues[1] = 0;
		}

		//If current soil level is less than limit
		if (getEnvironmentScript ().getSoilState() < getSoilLimit ()) {
			//Set soil issue element to 1
			issues[2] = 1;
		//If current soil level is greater than limit
		} else {
			//Set soil issue element to 0
			issues[2] = 0;
		}
	}

	/* Called when mouse clicks on plant
	Inverts selected variable, set selection halo
	Calls setSelectedPlant function in gameUISCript */
	void OnMouseDown (){
		if (PlantScript.isPaused) { return; }

		if(checkDelete()){
			return;
		}

		//Inverts selected variable
		selected = !selected;
		//Set selection halo based on whether selected or not
		//(gameObject.GetComponent("Halo") as Behaviour).enabled = selected;
		
		if (selected) {
			//If selected, call setSelectedPlant function with Game Object
			gameUIScript.setSelectedPlant (gameObject);
		} else {
			//If not selected, nullify setSelectedPlant function
			gameUIScript.setSelectedPlant (null);
		}
	}

	private bool checkDelete (){
		if (gameUIScript.getDeletePlant ()) {
			gameUIScript.setSelectedPlant (null);
			gameUIScript.setDeletePlant();
			environmentScript.setSoilState(environmentScript.getSoilState() + 0.5f);
			if (cousinGameObject != null) {
				Destroy (cousinGameObject);
			}
			Destroy (rootGameObject);
			return true;
		} else {
			return false;
		}
	}

	/* Called from dragPlant function
	Gets mouse coordinates in world coordinates and resets z to 0 for accurate tracking */
	private void getMousePosition (){
		mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
		mousePosition.z = 0;
	}
	
	//Getter method for sunLevel variable
	public float getSunLevel (){
		return sunLevel;
	}

	//Getter method for sunLimit variable
	public float getSunLimit (){
		return sunLimit;
	}

	//Getter method for waterLimit variable
	public float getWaterLimit (){
		return waterLimit;
	}

	//Getter method for soilLimit variable
	public float getSoilLimit (){
		return soilLimit;
	}

	//Getter method for status variable
	public int getStatus(){
		return status;
	}

	//Getter method for issues variable
	public int[] getIssues (){
		return issues;
	}

	//Getter method for environmentScript variable
	public EnvironmentScript getEnvironmentScript (){
		return environmentScript;
	}

	//Setter method for status variable
	public void setStatus(int aStatus){

		// It's dead jim!
		if (status == 3) { return; }

		status = aStatus;

		// We've started dying so keep track
		if (status == 2) {
			startedDying = Time.time;
		}
	}
}
