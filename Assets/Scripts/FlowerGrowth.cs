﻿using UnityEngine;
using System.Collections;

public class FlowerGrowth : FlowerScript {

	public Sprite[] stages = new Sprite[4];
	public GameObject flowerStem;
	public GameObject flowerHead;
	public GameObject flowerLeaf1;
	public GameObject flowerLeaf2;

	public Color[] headColors = new Color[5];

	GameObject stemChild;
	GameObject leaf1Child;
	GameObject leaf2Child;
	GameObject headChild;

	SpriteRenderer headRenderer;
	SpriteRenderer leaf1Renderer;
	SpriteRenderer leaf2Renderer;
	SpriteRenderer stemRenderer;

	FlowerBit headScript;
	FlowerBit leaf1Script;
	FlowerBit leaf2Script;
	FlowerBit stemScript;

	protected Color[] leafColors = {
		new Color(  0/255.0f, 165/255.0f, 22/255.0f),	// Growing
		new Color(  0/255.0f, 107/255.0f, 14/255.0f),	// Neutral
		new Color( 95/255.0f, 111/255.0f,  0/255.0f),	// Dying
		new Color(111/255.0f,  67/255.0f,  0/255.0f)	// Dead
	};

	// Override growth behavior
	protected override void grow () {

		if (curAge > 0 && curAge <= hitBoxDims.Length) {
			applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [curAge - 1]);
		}

		if (curAge < maturity) {
			// Set to next sprites
			childSpriteRender.sprite = stages [curAge - 1];
			updateAlpha (childSpriteRender, 1.0f);

			spriteRender.sprite = stages [curAge];
			updateAlpha (spriteRender, 0.0f);

			// Prepare to fade and request update
			alpha = 0.0f;
			updatingNeeded = true;

			// On last step, spawn in the mature replacements
			if (curAge == maturity - 1) {
				makeMature ();
			}

		} else {
			alpha = 1.0f;
			updatingNeeded = false;
			matureGrow ();
		}
	}

	protected void makeMature() {

		float globScale = 0.6f;

		// Create the children
		stemChild = spawnChild (flowerStem);
		stemChild.transform.position = transform.position;
		stemChild.transform.localScale = new Vector3 (1.5f*globScale, 1.5f*globScale, 1.5f*globScale);
		stemRenderer = stemChild.GetComponent<SpriteRenderer> ();
		stemRenderer.sortingOrder = mySortLevel + 1;
		stemScript = stemChild.GetComponent<FlowerBit> ();
		stemScript.mySortLevel = mySortLevel + 1;
		stemScript.setActiveSprite (2);

		float fudge = Random.value * 2 - 1;

		leaf1Child = spawnChild (flowerLeaf1);
		leaf1Child.transform.position = transform.position;
		leaf1Child.transform.Translate (new Vector3 (-0.4f*globScale, 4.5f*globScale+fudge, 0.0f*globScale));
		leaf1Child.transform.localScale = new Vector3 (globScale, globScale, globScale);
		leaf1Renderer = leaf1Child.GetComponent<SpriteRenderer> ();
		leaf1Renderer.sortingOrder = mySortLevel;
		leaf1Script = leaf1Child.GetComponent<FlowerBit> ();
		leaf1Script.mySortLevel = mySortLevel;
		leaf1Script.setActiveSprite (2);

		fudge = Random.value * 2 - 1;

		leaf2Child = spawnChild (flowerLeaf2);
		leaf2Child.transform.position = transform.position;
		leaf2Child.transform.Translate (new Vector3 (0.0f*globScale, 4.0f*globScale+fudge, 0.0f*globScale));
		leaf2Child.transform.localScale = new Vector3 (globScale, globScale, globScale);
		leaf2Renderer = leaf2Child.GetComponent<SpriteRenderer> ();
		leaf2Renderer.sortingOrder = mySortLevel;
		leaf2Script = leaf2Child.GetComponent<FlowerBit> ();
		leaf2Script.mySortLevel = mySortLevel;
		leaf2Script.setActiveSprite (2, true);

		headChild = spawnChild (flowerHead);
		headChild.transform.position = transform.position;
		headChild.transform.Translate (new Vector3 (0.0f*globScale, 8.8f*globScale, 0.0f*globScale));
		headChild.transform.localScale = new Vector3 (2.0f*globScale, 2.0f*globScale, 2.0f*globScale);
		headRenderer = headChild.GetComponent<SpriteRenderer> ();
		headRenderer.sortingOrder = mySortLevel+2;

		int headChoice = Mathf.RoundToInt (Random.value * 6.0f + 1.0f);
		headScript = headChild.GetComponent<FlowerBit> ();
		headScript.setActiveSprite (headChoice);
		headRenderer.color = headColors [headChoice - 1];
		headScript.mySortLevel = mySortLevel+2;
	}

	protected void matureGrow() {

		switch (getStatus()) {
			case 0: // growing
				leaf1Script.setActiveSprite (1);
				leaf2Script.setActiveSprite (1, true);
				leaf1Renderer.color = leafColors [0];
				leaf2Renderer.color = leafColors [0];
				break;

			case 1: // neutral
				leaf1Script.setActiveSprite (2);
				leaf2Script.setActiveSprite (2, true);
				leaf1Renderer.color = leafColors [1];
				leaf2Renderer.color = leafColors [1];
				break;

			case 2: // Dying
				leaf1Script.setActiveSprite (3);
				leaf2Script.setActiveSprite (3, true);
				leaf1Renderer.color = leafColors [2];
				leaf2Renderer.color = leafColors [2];
				break;

			case 4: // Dead
				leaf1Script.setActiveSprite (3);
				leaf2Script.setActiveSprite (3, true);
				leaf1Renderer.color = leafColors [3];
				leaf2Renderer.color = leafColors [3];
				break;
		}
	}
		
	// Provide specialized fixed update logic
	protected override void fixedTick() {

		if (alpha < 1.0f) {
			blendStep ();
		} else {
			resetBlend ();
			updatingNeeded = false;

			if (curAge >= 0 && curAge < hitBoxDims.Length) {
				applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [curAge]);
			}
		}
	}

}
