﻿using UnityEngine;
using System.Collections;

/**
 * Base class for all growable plants.  They can evolve over time
 * through stages and eventually reach maturity when they will bear
 * fruit or seeds.  The time between growth phases and odds of
 * growing are all tunable.
 */
public abstract class GrowablePlant : PlantScript {

	// Stage at which this plant has reached maturity
	public int maturity = int.MaxValue;

	// Current stage of this plant
	public int curAge = 0;

	// Number of attached children and depth in ancestry tree
	public int childCount = 0;
	public int depth = 0;

	// Maximum allowed children and depth of ancestry tree
	public int maxChildCount = int.MaxValue;
	public int maxDepth = int.MaxValue;

	// Change of growth event occuring
	public float growthOdds = 0.5f;

	// Time this plant was born
	public float birth;

	// Position and dims for the 2dColliderBoxes
	public Vector4[] hitBoxDims = new Vector4[5];

	// Time range during which a growth event might occur [min, max]
	public float[] timeBeforeGrowth = new float[2];

	// Last time this plant grew
	public float lastGrowth;

	// Call the tick function every fixed update
	protected bool updatingNeeded = false;

	public override void init() {
		base.init ();
		applyHitBoxDims (GetComponent<BoxCollider2D> (), hitBoxDims [0]);
		GetComponent<BoxCollider2D> ().enabled = !dragable;
	}

	protected override void doneDragging ()
	{
		lastGrowth = birth = Time.time;
		GetComponent<BoxCollider2D> ().enabled = true;
	}

	void FixedUpdate ()
	{
		if (PlantScript.isPaused) { return; }

		// Don't grow until done with dragging
		if (dragging) return;
		
		// Are we in the target growth range
		float elapsed = Time.time - lastGrowth;
		if (elapsed > timeBeforeGrowth[0]) {

			// Try to grow
			if ((elapsed > timeBeforeGrowth[1]) || Random.value > (1.0 - growthOdds))
			{
				// Success, let's grow
				if (curAge <= maturity) {
					lastGrowth = Time.time;
					curAge++;
				}
				grow ();
			}
		}

		// Check for tick update
		if (updatingNeeded) {
			fixedTick ();
		}
	}

	// These are made more specific in the children
	protected abstract void grow ();
	protected virtual void fixedTick() {}

	protected void applyHitBoxDims (BoxCollider2D box, Vector4 dims, bool flipped = false) {
		if (box == null) return;

		float x = dims.x;
		if (flipped) { x += dims.z; }
		box.offset = new Vector2 (x, dims.y);
		box.size = new Vector2 (dims.z, dims.w);
	}

	/**
	 * Generic function to spawn a child instance properly.  Will parent to
	 * this object and have properly initialized GrowablePlant variables.
	 * @param plantPrefab A reference to a GameObject prefab with an attached GrowablePlant script component.
	 * @return A new GameObject that matches the parameter and is a child of this object.
	 */
	public GameObject spawnChild(GameObject plantPrefab)
	{
		// Spawn a child
		GameObject child = Instantiate (plantPrefab);
		GrowablePlant childGen = child.GetComponent<GrowablePlant> ();
		childCount++;

		// Setup script variables
		childGen.childCount = 0;
		childGen.curAge = 0;
		childGen.depth = depth + 1;
		childGen.maxDepth = maxDepth;

		// Copy our root game object (might be this, might be our parent)
		childGen.RootGameObject = rootGameObject;

		// Parent to this object and reset scale
		child.transform.parent = gameObject.transform;
		child.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);

		// Return the newly formed child
		return child;
	}
}
